#
# Sq1090 - high-level API to parse and publish ADS-B messages
#
# Copyright (C) 2018-2024 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

- hosts: test-image
  tasks:
    - name: "system: install distro managed packages"
      become: true
      package:
        name:
          - postgis
          - postgresql
          - python-asyncpg
          - python-cytoolz
          - python-psycopg2
          - python-requests
          - python-uvloop
          - rabbitmq
          - rtl-sdr
          - timescaledb
          - ufw

    - name: "system: copy dump1090 package"
      copy:
        src: "{{ dump1090_pkg }}"
        dest: "/tmp/{{ dump1090_pkg }}"

    - name: "system: install dump1090 package"
      become: true
      community.general.pacman:
        name: "/tmp/{{ dump1090_pkg }}"
        state: present

    - name: "system: setup firewall"
      become: true
      ufw:
        state: enabled
        port: ssh
        rule: allow

    - name: "database: setup cluster"
      become: true
      become_user: postgres
      command:
        cmd: initdb -E utf8 -D /var/lib/postgres/data
        creates: /var/lib/postgres/data/PG_VERSION

    - name: "system: start service"
      become: yes
      systemd:
        name: "{{ item }}"
        enabled: yes
        daemon_reload: yes
        state: started
      loop: "{{ sq1090_stack }}"

    - name: "system: sq1090 user"
      become: yes
      user:
        name: sq1090
        groups: uucp
        shell: /bin/bash

    - name: "database: sq1090 user"
      become: yes
      become_user: postgres
      postgresql_user:
        name: sq1090
        role_attr_flags: "NOSUPERUSER,LOGIN"

    - name: "streams: extensions"
      become: yes
      community.rabbitmq.rabbitmq_plugin:
        names: "{{ item }}"
      loop: ["rabbitmq_amqp1_0", "rabbitmq_stream", "rabbitmq_management"]

    - name: "streams: sq1090 user"
      become: yes
      community.rabbitmq.rabbitmq_user:
        user: sq1090
        password: "{{ sq1090_password }}"
        permissions:
          - vhost: /
            configure_priv: ^sq1090.*
            read_priv: ^sq1090.*
            write_priv: ^sq1090.*

    - name: "streams: create"
      become: yes
      community.rabbitmq.rabbitmq_queue:
        name: "{{ item }}"
        arguments:
          x-queue-type: stream
          x-max-age: 90D
      loop: "{{ sq1090_streams }}"

    - name: 'sq1090: create config directory'
      become: yes
      file: 
        path: /etc/sq1090
        state: directory

    - name: "sq1090: create config file"
      become: yes
      template:
        src: sq1090.cfg.j2
        dest: /etc/sq1090/sq1090.cfg

    - name: "sq1090: create service file"
      become: yes
      template:
        src: "{{ item.template }}"
        dest: "/etc/systemd/system/{{ item.name }}.service"
      with_items: "{{ sq1090_services }}"

    - name: "sq1090: start service"
      become: yes
      systemd:
        name: "{{ item.name }}"
        enabled: yes
        daemon_reload: yes
        state: restarted
      loop: "{{ sq1090_services }}"

# vim: sw=2:et:ai
