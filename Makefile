.PHONY = test-img test-img-deploy clean

DUMP1090_PKG := dump1090-9.0-1-x86_64.pkg.tar.zst

TEST_IMAGE_URL := https://geo.mirror.pkgbuild.com/images
TEST_IMAGE_VER := 20240415.229275
TEST_IMAGE_BASE := Arch-Linux-x86_64-basic-$(TEST_IMAGE_VER).qcow2
TEST_IMAGE := arch-linux-test.qcow2

SSH_PORT := 10022
SSH := sshpass -p arch \
	   ssh \
	   		-o StrictHostKeyChecking=no \
	   		-o UserKnownHostsFile=/dev/null \
			-p $(SSH_PORT) \
			arch@localhost --
SSH_SUDO := $(SSH) sudo

test-img-run: .stamp-test-img
	qemu-system-x86_64 \
		-enable-kvm \
		-smp 1 -m 2G \
		-nic user,model=virtio-net-pci,hostfwd=tcp::$(SSH_PORT)-:22 \
		-device virtio-blk,drive=hd0 \
		-drive if=none,file=$(TEST_IMAGE),id=hd0

test-img-deploy: .stamp-test-img-bootstrap ansible/sq1090-test-image.yaml
	cd ansible; \
		ansible-playbook sq1090-test-image.yaml -e '@vault.yaml' -e '@vars-service.yaml' -e '@vars.yaml'

dump1090: ansible/$(DUMP1090_PKG)

.stamp-test-img-bootstrap: .stamp-test-img
	$(SSH_SUDO) pacman-key --init
	$(SSH_SUDO) pacman-key --populate archlinux
	$(SSH_SUDO) pacman -Syu --noconfirm python
	$(SSH) mkdir -p .ssh
	$(SSH_SUDO) "cat > .ssh/authorized_keys" < ~/.ssh/id_rsa.pub
	$(SSH_SUDO) usermod -L arch
	
	$(SSH_SUDO) reboot  # kernel might be reinstalled, reboot to allow use of its modules
	sleep 10  # let the machine to reboot
	touch .stamp-test-img-bootstrap

.stamp-test-img: $(TEST_IMAGE_BASE)
	cp $(TEST_IMAGE_BASE) $(TEST_IMAGE)
	touch .stamp-test-img

$(TEST_IMAGE_BASE):
	wget $(TEST_IMAGE_URL)/v$(TEST_IMAGE_VER)/$(TEST_IMAGE_BASE)

ansible/$(DUMP1090_PKG): dump1090/$(DUMP1090_PKG)
	cp dump1090/$(DUMP1090_PKG) ansible

dump1090/$(DUMP1090_PKG): dump1090/PKGBUILD dump1090/dump1090.install
	cd dump1090; makepkg -s

clean:
	rm -f .stamp-* $(TEST_IMAGE)
